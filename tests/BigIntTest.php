<?php

declare(strict_types=1);

namespace Grifix\BigInt\Tests;

use Grifix\BigInt\BigInt;
use Grifix\BigInt\Exceptions\NotIntegerValueException;
use Grifix\BigInt\Exceptions\NotNumericValueException;
use PHPUnit\Framework\TestCase;

final class BigIntTest extends TestCase
{
    /**
     * @dataProvider itCreatesDataProvider
     */
    public function testItCreates(mixed $value, string $expectedValue): void
    {
        $bigInt = BigInt::create($value);
        self::assertEquals($expectedValue, $bigInt->toString());
        self::assertEquals($expectedValue, (string)$bigInt);
    }

    public function itCreatesDataProvider(): array
    {
        return [
            [1, '1',],
            [1.0, '1'],
            ['1', '1'],
            ['1.0', '1'],
            [1e8, '100000000'],
            [1.1e2, '110']
        ];
    }

    /**
     * @dataProvider itDoesNotCreateProvider
     */
    public function testItDoesNotCreate(mixed $value, string $expectedException, string $expectedMessage): void
    {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedMessage);
        BigInt::create($value);
    }

    public function itDoesNotCreateProvider(): array
    {
        return [
            [
                '1.1',
                NotIntegerValueException::class,
                'Value [1.1] is not integer!'
            ],
            [
                '0.1',
                NotIntegerValueException::class,
                'Value [0.1] is not integer!'
            ],
            [
                1.5,
                NotIntegerValueException::class,
                'Value [1.5] is not integer!'
            ],
            [
                '1a',
                NotNumericValueException::class,
                'Value [1a] is not numeric!'
            ],
            [
                'a1',
                NotNumericValueException::class,
                'Value [a1] is not numeric!'
            ],
            [
                '1.1.1',
                NotNumericValueException::class,
                'Value [1.1.1] is not numeric!'
            ]

        ];
    }

    /**
     * @dataProvider itEqualToDataProvider
     */
    public function testItEqualTo(BigInt $value, mixed $other, bool $expectedResult): void
    {
        self::assertEquals(
            $expectedResult,
            $value->isEqualTo($other)
        );
    }

    public function itEqualToDataProvider(): array
    {
        return [
            [BigInt::create(1), 1, true],
            [BigInt::create(1), BigInt::create(1), true],
            [BigInt::create(1), 1.0, true],
            [BigInt::create(1), '1', true],
            [BigInt::create(1), '2', false],
            [BigInt::create(1), 2, false],
            [BigInt::create(1), 2.0, false],
            [BigInt::create(1), BigInt::create(2), false],
        ];
    }

    /**
     * @dataProvider itGreaterThanDataProvider
     */
    public function testItGreaterThan(BigInt $value, mixed $other, bool $expectedResult): void
    {
        self::assertEquals(
            $expectedResult,
            $value->isGreaterThan($other)
        );
    }

    public function itGreaterThanDataProvider(): array
    {
        return [
            [BigInt::create(1), 1, false],
            [BigInt::create(1), BigInt::create(1), false],
            [BigInt::create(1), 1.0, false],
            [BigInt::create(1), '1', false],
            [BigInt::create(1), 2, false],
            [BigInt::create(1), BigInt::create(2), false],
            [BigInt::create(1), 2.0, false],
            [BigInt::create(1), '2', false],
            [BigInt::create(2), '1', true],
            [BigInt::create(2), 1, true],
            [BigInt::create(2), 1.0, true],
            [BigInt::create(2), BigInt::create(1), true],
        ];
    }

    /**
     * @dataProvider itLessThanDataProvider
     */
    public function testItLessThan(BigInt $value, mixed $other, bool $expectedResult): void
    {
        self::assertEquals(
            $expectedResult,
            $value->isLessThan($other)
        );
    }

    public function itLessThanDataProvider(): array
    {
        return [
            [BigInt::create(1), 1, false],
            [BigInt::create(1), BigInt::create(1), false],
            [BigInt::create(1), 1.0, false],
            [BigInt::create(1), '1', false],
            [BigInt::create(1), 2, true],
            [BigInt::create(1), BigInt::create(2), true],
            [BigInt::create(1), 2.0, true],
            [BigInt::create(1), '2', true],
            [BigInt::create(2), '1', false],
            [BigInt::create(2), 1, false],
            [BigInt::create(2), 1.0, false],
            [BigInt::create(2), BigInt::create(1), false],
        ];
    }

    /**
     * @dataProvider itLessOrEqualThanDataProvider
     */
    public function testItLessOrEqualThan(BigInt $value, mixed $other, bool $expectedResult): void
    {
        self::assertEquals(
            $expectedResult,
            $value->isLessOrEqualThan($other)
        );
    }

    public function itLessOrEqualThanDataProvider(): array
    {
        return [
            [BigInt::create(1), 1, true],
            [BigInt::create(1), BigInt::create(1), true],
            [BigInt::create(1), 1.0, true],
            [BigInt::create(1), '1', true],
            [BigInt::create(1), 2, true],
            [BigInt::create(1), BigInt::create(2), true],
            [BigInt::create(1), 2.0, true],
            [BigInt::create(1), '2', true],
            [BigInt::create(2), '1', false],
            [BigInt::create(2), 1, false],
            [BigInt::create(2), 1.0, false],
            [BigInt::create(2), BigInt::create(1), false],
        ];
    }

    /**
     * @dataProvider itAddsDataProvider
     */
    public function testItAdds(BigInt $value, mixed $other, string $expectedResult): void
    {
        self::assertEquals($expectedResult, $value->add($other)->toString());
    }

    public function itAddsDataProvider(): array
    {
        return [
            [BigInt::create(2), 2, '4'],
            [BigInt::create(2), '2', '4'],
            [BigInt::create(2), 2, '4'],
            [BigInt::create(2), '2.0', '4'],
            [BigInt::create(2), 2.0, '4'],
            [BigInt::create(2), BigInt::create(2), '4'],
        ];
    }

    /**
     * @dataProvider itSubstratesDataProvider
     */
    public function testItSubstrates(BigInt $value, mixed $other, string $expectedResult): void
    {
        self::assertEquals($expectedResult, $value->subtract($other)->toString());
    }

    public function itSubstratesDataProvider(): array
    {
        return [
            [BigInt::create(4), 2, '2'],
            [BigInt::create(4), '2', '2'],
            [BigInt::create(4), 2, '2'],
            [BigInt::create(4), '2.0', '2'],
            [BigInt::create(4), 2.0, '2'],
            [BigInt::create(4), BigInt::create(2), '2'],
        ];
    }

    public function testItMultiplies(): void
    {
        self::assertEquals('25', BigInt::create(5)->multiple(5)->toString());
    }
}
