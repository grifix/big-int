<?php
declare(strict_types=1);

namespace Grifix\BigInt\Exceptions;

final class NotNumericValueException extends \Exception
{
    public function __construct(public readonly string $value)
    {
        parent::__construct(sprintf('Value [%s] is not numeric!', $value));
    }
}
