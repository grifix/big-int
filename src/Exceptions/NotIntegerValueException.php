<?php
declare(strict_types=1);

namespace Grifix\BigInt\Exceptions;

final class NotIntegerValueException extends \Exception
{

    public function __construct(public readonly float $value)
    {
        parent::__construct(sprintf('Value [%s] is not integer!', $value));
    }
}
